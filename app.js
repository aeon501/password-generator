const copy = document.getElementById("copy");
const numbers = document.getElementById("numbers");
const lowercase = document.getElementById("lowercase");
const uppercase = document.getElementById("uppercase");
const symbol = document.getElementById("symbols");
const generate = document.getElementById("generate");
const passwordLength = document.getElementById("passwordLength");
const password = document.getElementById("password");
const echo = console.log;

const Generator = {
  lower: getLowerCase,
  upper: getUpperCase,
  number: getNumbers,
  symbol: getSymbols
};

/**
 * @function getNumbers
 * @return {string} {a random string of a number}
 */
function getNumbers() {
  const numerals = "0123456789";
  return numerals[Math.floor(Math.random() * numerals.length)];
  //return String.fromCharCode(Math.floor(Math.random() * 10) + 48);
}

/**
 * @function getLowerCase
 * @return {string} {a random string of a lowercase}
 */
function getLowerCase() {
  const lowerCase = "abcdefghijklmnopqrstuvwxyz";
  return lowerCase[Math.floor(Math.random() * lowerCase.length)];
  //return String.fromCharCode(Math.floor(Math.random() * 26) + 97);
}

/**
 * @function getUpperCase
 * @return {string} {random string of uppercase alphabets}
 */
function getUpperCase() {
  const upperCase = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
  return upperCase[Math.floor(Math.random() * upperCase.length)];
  //return String.fromCharCode(Math.floor(Math.random() * 26) + 65);
}

/**
 * @function getSymbols
 * @return {string} {random string of symbols}
 */
function getSymbols() {
  const symbolList = "!@#$%^&*~;:()_+/><";
  return symbolList[Math.floor(Math.random() * symbolList.length)];
}

generate.addEventListener("click", () => {
  const length = +passwordLength.value;
  const hasNumber = numbers.checked;
  const hasLower = lowercase.checked;
  const hasUpper = uppercase.checked;
  const hasSymbols = symbol.checked;

  passwordGenerator(hasLower, hasUpper, hasNumber, hasSymbols, length);
});

/**
 * @function passwordGenerator
 * @param  {boolean} lower  {lowercase case}
 * @param  {boolean} upper  {upper case}
 * @param  {boolean} number {numerals}
 * @param  {boolean} symbol {symbols}
 * @param  {string} length {password string length}
 * @return {string} {generated string}
 */
function passwordGenerator(lower, upper, number, symbol, length) {
  let generatedPassword = "";
  const typeCount = lower + upper + number + symbol;
  const typeArray = [{ lower }, { upper }, { number }, { symbol }].filter(
    item => Object.values(item)[0]
  );

  if (typeCount === 0) {
    return "";
  }
  for (let i = 0; i < length; i += typeCount) {
    typeArray.forEach(type => {
      const passwordGenerator = Object.keys(type)[0];

      generatedPassword += Generator[passwordGenerator]();
    });
  }

  //convert string to array
  let finalpassword = generatedPassword.slice(0, length);

  password.value = finalpassword;
}

copy.addEventListener("click", () => {
  copyText();
});

/**
 * @function copyText
 * @return {string} {copy the generated password to clipboard}
 */
function copyText() {
  /* Get the text field */
  let Text = document.getElementById("password");

  /* Select the text field */
  Text.select();
  Text.setSelectionRange(0, 99999); /*For mobile devices*/

  /* Copy the text inside the text field */
  document.execCommand("copy");

  /* Alert the copied text */
  alert("Copied the text: " + Text.value);
}
